Query Operators

// $gt and $gte operator (greater than/greater than or equal to)

// find a users with an age greater than 50
db.users.find({
	age: {
		$gt: 50
	}
})

// find a users with an age greater than or equal to 50
db.users.find({
	age: {
		$gte: 50
	}
})

// find a users with an age less than 50
db.users.find({
	age: {
		$lt: 50
	}
})

// find a users with an age less than or equal to 50
db.users.find({
	age: {
		$lte: 50
	}
})

// find users with an age that is NOT euqal to 82

db.users.find({
	age: {
		$ne: 82
	}
})

// find users whose last names are either "Hawking" or "Doe"
db.users.find({
	lastName: {
		$in: ["Hawking", "Doe"]
	}
})

// find users whose courses including "HTML" or "React"
db.users.find({
	courses: {
		$in: ["HTML", "React"]
	}
})

// [section] Logical Query Operators

// $or operator
db.users.find({
	$or: [
	{
		firstName: "Neil"
	},
	{
		age: 21
	}
	]
})

// $and operator
db.users.find({
		$and: [
		{
			age: {
				$ne: 82
			}
		},
		{
			age: {
				$ne: 76
			}
		}
	]
})
